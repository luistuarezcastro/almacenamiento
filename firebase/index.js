import { initializeApp } from "firebase/app";
import {getAuth} from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyATQDNflAU55VkRNqjuyRMmeRg_er_8oF4",
  authDomain: "nuevo-6fad2.firebaseapp.com",
  projectId: "nuevo-6fad2",
  storageBucket: "nuevo-6fad2.appspot.com",
  messagingSenderId: "102223597383",
  appId: "1:102223597383:web:6b583e17093d4f69e70662"
};

const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export {auth};
